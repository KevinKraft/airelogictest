'''
Script to initialise databases with fixed information

Kevin Maguire
26/03/19

'''

#------------------------------------------------------------------------------------------------

import os, sys
sys.path.append( os.path.dirname(os.path.abspath(__file__))+"/../src" )
from datetime import datetime

import util.message as msg
import util.utilities as ut
from database.database import DDatabase, TIME_TYPE

#------------------------------------------------------------------------------------------------

def main():

    #make a new blank db in the data folder.
    cwd = os.getcwd()
    db = DDatabase(cwd+"/../data/database.db", overwrite=True)

    #create a blank table for the issue tracking info
    tname = "issue_tracker"
    db.createTable("issue_tracker", [
        ("title", str),
        ("description", str),
        ("timestamp", TIME_TYPE),
        ("assigned_to", str),
        ("status", str),
    ])

    #add some data
    db.addData( tname, ("First issue","Issue description.",ut.getStringTime(),"No user","open") )
    db.addData( tname, ("Second issue","Issue description 2.",ut.getStringTime(),"No user","open") )
    db.addData( tname, ("Third issue","Issue description 3.",ut.getStringTime(),"No user","open") )
    db.addData( tname, ("Fourth issue","Issue description 4.",ut.getStringTime(),"No user","open") )
    db.addData( tname, ("Fifth issue","Issue description 5.",ut.getStringTime(),"No user","open") )

    #print it
    print(db.dataframe(tname))

if __name__ == "__main__":
    main()

