'''
Tests of the message class.

Kevin Maguire
26/03/19

'''

#------------------------------------------------------------------------------------------------

import os, sys
sys.path.append( os.path.dirname(os.path.abspath(__file__))+"/../src" )
import unittest

from util.message import CMessage, DEBUG, WARN, ERROR, INFO

#------------------------------------------------------------------------------------------------

class MessageTest(unittest.TestCase):
    def test(self):
        m = CMessage(DEBUG)
        self.assertEqual( m.log("Initial message.",ERROR), "ERRO: Initial message.",
                          "Should be 'ERRO: Initial message.'")

    def testNone(self):
        m = CMessage(WARN)
        self.assertEqual( m.log("Temp message.",DEBUG), None, "Should be None")
        self.assertEqual( m.log("Temp message.",INFO), None, "Should be None")
        self.assertEqual( m.log("Temp message.",WARN), "WARN: Temp message.",
                          "Should be 'WARN: Temp Message.'")

    def testFName(self):
        m = CMessage(WARN)
        self.assertEqual( m.log("Temp message.",WARN, "MessageTest::testFName"),
                          "WARN: MessageTest::testFName: Temp message.",
                          "Should be 'WARN: MessageTest::testFName: Temp message.'")
