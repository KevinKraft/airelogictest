'''
Tests of the utilities functions.

Kevin Maguire
26/03/19

'''

#------------------------------------------------------------------------------------------------

import os, sys
sys.path.append( os.path.dirname(os.path.abspath(__file__))+"/../src" )
import unittest
from datetime import datetime

import util.utilities as ut

#------------------------------------------------------------------------------------------------

class UtilitiesTimeTest(unittest.TestCase):
    def test(self):
        self.assertEqual( isinstance(ut.getTime(), datetime), True, "Should be True")
        self.assertEqual( isinstance(ut.getStringTime(), str), True, "Should be True")
        self.assertEqual( isinstance(ut.convertStringToTime(ut.getStringTime()), datetime), True,
                          "Should be True")
