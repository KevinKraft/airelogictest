'''
Main function for all tests.

Kevin Maguire
26/03/19

'''

#------------------------------------------------------------------------------------------------

import unittest

from tests.message_test import MessageTest
from tests.utilities_test import UtilitiesTimeTest

#------------------------------------------------------------------------------------------------

def main():
    unittest.main()

if __name__ == "__main__":
    main()