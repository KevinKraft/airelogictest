# AirelogicTest

# Installation

Use anaconda

	conda create --name airlogic python=3.6

The three packages you will need are:

	conda install pandas
	conda install aiohttp
	conda install python-socketio
	
# Usage
	
Run server.py.

Open browser to localhost:8080.

Press the load button near the bottom to load preexisting issues.

Multiple clients will have the issue list updated at the same time.

# Below here are some random notes.

# Initial thoughts:

	• From scratch python
		○ Databases
			§ I havent used them
			§ Instead read from a CSV into a pandas DF
			§ A SQLite DB can be read into a pandas DF, so this could be replaced with a proper DB.
			§ Just use sqlite3 in python
		○ Initial system
			§ Make it a terminal based system before doing any webstuff
		○ Webstuff
			§ Use python websocket server to transfer info
			§ Will need to use threads on the backend to make it non-blocking
				□ This is a later stage thing which I might not have time to do.
			§ I don't want to use node.js as I'm not hugely comfortable with it.
		○ Webpages
			§ I don't have much experience of making nice web pages.
			§ I'll probably have a single page with boxes for editing and adding the information
				□ It can use issue Ids to make changes
			§ Then I'll have a page that displays all the issues.
				□ This could also be the same page.
			§ I could add a page for each issue but I probably won't have time
				□ If I did this then the issue ID wouldn't be necessary when making changes, it could be done on the issues own web page.

	• Issue tracking system bug fixing
		○ I think i'd be able to do this in less time than the from scratch task will take.

# Code layout:

Database system

Issue classes and main system

Terminal commands for editing issues

Webserver frontend with socketio

# Update/Thoughts:

I spent too long doing SQL stuff as I have to learn how to use it.

The importing is a bit messed up, I probably wont have time to do it correctly.

I setup the Ddatabase class to have multiple tables so I could eventually add a Users table also.

I don't seem to have time to make test cases.




	

