'''
Main function.

Kevin Maguire
26/03/19

'''

#------------------------------------------------------------------------------------------------

import os, sys
sys.path.append( os.path.dirname(os.path.abspath(__file__))+"/src" )

from util.message import message
import util.utilities as ut
from database.database import DDatabase
from issue_tracker.issue_tracker import CIssueTracker

#------------------------------------------------------------------------------------------------

def main():

    tname = "issue_tracker"

    #open the preexisting db
    cwd = os.getcwd()
    db = DDatabase(cwd+"/data/database.db", overwrite=False)
    print(db)
    df = db.dataframe(tname)
    print(df)

    #add an entry
    message.displayBreak()
    nid = db.addData( tname, ("Temp issue","Issue to be deleted.",ut.getStringTime(),"No user","open") )
    print(db.dataframe(tname))

    #check if data exists
    message.displayBreak()
    print(db.hasData(tname, "title", "Temp issue"))
    print(db.hasData(tname, "title", "blah"))

    #remove an issue
    message.displayBreak()
    print(db.dataframe(tname))
    db.deleteData(tname, nid)
    print(db.dataframe(tname))

    #update cell data
    message.displayBreak()
    nid = db.addData( tname, ("Edit issue","Issue to be edited.",ut.getStringTime(),"No user","open") )
    db.updateCell(tname, nid, "status", "closed")
    print(db.dataframe(tname))

    #make an issue tracker
    message.displayBreak()
    path = os.getcwd()+"/data/issue_tracker.db"
    it = CIssueTracker(path, overwrite=True)
    print(it)
    print(it.dataframe(tname))

    #add an issue
    message.displayBreak()
    it.newIssue("A new issue", "issue description.")
    print(it.dataframe(tname))

    #delete an issue
    message.displayBreak()
    nid = it.newIssue("Paint red.", "paint the wall red.")
    print(it.dataframe(tname))
    it.deleteIssue(nid)
    print(it.dataframe(tname))

    #get some data from the db
    message.displayBreak()
    nid = it.newIssue("Paint yellow.", "paint the wall yellow.")
    title = it.getData(tname, "title", 1)
    print(title)

    #query an issue object.
    message.displayBreak()
    nid = it.newIssue("Paint blue.", "paint the wall blue.")
    io = it.getIssue(nid)
    print(io)

    #close and open issue
    message.displayBreak()
    it.closeIssue(nid)
    print(it.dataframe(tname))
    it.openIssue(nid)
    print(it.dataframe(tname))

    #iterate through ids
    message.displayBreak()
    for id in it:
        print(id)

    #get issue data list
    message.displayBreak()
    print(it.getIssueList())



if __name__ == "__main__":
    main()