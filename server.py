'''
Main function running the server and the system

Kevin Maguire
26/03/19

'''

#------------------------------------------------------------------------------------------------

import os, sys
sys.path.append( os.path.dirname(os.path.abspath(__file__))+"/src" )

from aiohttp import web
import socketio

from util.message import message
import util.utilities as ut
from issue_tracker.issue_tracker import CIssueTracker

#------------------------------------------------------------------------------------------------

#setup the server, it is on localhost:8080
sio = socketio.AsyncServer()
app = web.Application()
sio.attach(app)

#the issue tracker
ITRACKER = CIssueTracker(os.getcwd()+"/data/issue_tracker.db", overwrite=False)

#------------------------------------------------------------------------------------------------

async def index(request):
    '''Function to send the initial HTML.'''
    with open('template/index.html') as f:
        return web.Response(text=f.read(), content_type='text/html')

app.router.add_get('/', index)

# @sio.on('message')
# async def print_message(sid, message):
#     print("Socket ID: " , sid)
#     print(message)
#     # await a successful emit of our reversed message
#     # back to the client
#     await sio.emit('message', message[::-1])

# @sio.on('messageData')
# async def messageData(sid, data):
#     print("Socket ID: " , sid)
#     print(data)
#     # await a successful emit of our reversed message
#     # back to the client
#     await sio.emit('update', data)

@sio.on("connect")
def onConnection(sid, _):
    message.logInfo("Socket: "+str(sid)+" connected.", )

@sio.on("makeNewIssue")
async def makeNewIssue(sid, title, desc):
    ITRACKER.newIssue(title, desc)
    print(ITRACKER)
    await sio.emit("update", ITRACKER.getIssueList())

@sio.on("load")
async def load(sid):
    await sio.emit("update", ITRACKER.getIssueList())

@sio.on("closeIssue")
async def closeIssue(sid, id):
    ITRACKER.closeIssue(id)
    await sio.emit("update", ITRACKER.getIssueList())

@sio.on("openIssue")
async def openIssue(sid, id):
    ITRACKER.openIssue(id)
    await sio.emit("update", ITRACKER.getIssueList())

@sio.on("deleteIssue")
async def deleteIssue(sid, id):
    ITRACKER.deleteIssue(int(id))
    await sio.emit("update", ITRACKER.getIssueList())

def main():

    web.run_app(app)

if __name__ == '__main__':
    main()

