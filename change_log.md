## Change log

Kevin Maguire 26/03/19 17:46
----------------------------------------------

Made a simple webserver main function. Added the database functions 
to a very simple webpage. The displayed issues updates for all clients 
at once, which is nice. But I expect the backend to be blocking, it is 
just so fast that you don't notice. I added some of my notes to the README. 
I added a few checks so invalid ID inputs are ignored. 

Kevin Maguire 26/03/19 15:28
----------------------------------------------

Added functions for opening and closing issues.

Kevin Maguire 26/03/19 15:17
----------------------------------------------

Added an issue tracker class. It is a child class of a database that 
has essentially renamed methods and a few extra checks to make sure 
the given cell data has the correct type and value. Also added an 
Issue class. It is a simple way to view a single issue, but it doesn't 
update in real-time so it is read-only, and not very useful.

Kevin Maguire 26/03/19 13:37
----------------------------------------------

Learned how to use SQL. Added a database class that contains the SQL 
functionality. I haven't added unit tests. I don't think I will have 
time to add them, unless I do it at the end.

Kevin Maguire 26/03/19 10:07
----------------------------------------------

Added a simple utils file a simple message system, tests and directory 
structures.