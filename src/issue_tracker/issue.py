'''
Class for holding issue information.

Kevin Maguire
26/03/19

'''

#------------------------------------------------------------------------------------------------

import util.message as msg
import util.utilities as ut

#------------------------------------------------------------------------------------------------

class DIssue(object):
    '''
    Class for holding issue information. This has no access to the database, so it is read only.
    '''
    def __init__(self, id, title, desc, ts, assigned_to, status):
        self._id = id
        self._title = title
        self._desc = desc
        self._ts = ts
        self._assignedTo = assigned_to
        self._status = status

    def __str__(self):
        ret = "Issue: "+str(self._id)+": "+self._title+"\n"
        ret += "  "+self._ts+"\n"
        ret += "    "+self._desc+"\n"
        ret += "  Assigned to: "+self._assignedTo+"\n"
        ret += "  Status: "+self._status+"\n"
        return ret

    def __eq__(self, other):
        return self._id == other.getID()

    #------------------------------------------------------------------------------------------------
    # Public members
    #------------------------------------------------------------------------------------------------

    def getID(self): return self._ID

    def getTitle(self): return self._title

    def getDescription(self): return self._desc

    def getTimeStamp(self): return self._ts

    def getAssignedTo(self): return self._assignedTo

    def getStatus(self): return self._status