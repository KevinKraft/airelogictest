'''
Class for the issue tracker.

Kevin Maguire
26/03/19

'''

#------------------------------------------------------------------------------------------------

import util.message as msg
import util.utilities as ut
from database.database import DDatabase, TIME_TYPE, ID_COL_NAME
from .issue import DIssue

#------------------------------------------------------------------------------------------------

TITLE_COL = "title"
DESC_COL = "description"
TIMESTAMP_COL = "timestamp"
ASSIGNED_TO_COL = "assigned_to"
STATUS_COL = "status"
READ_ONLY_COLUMNS = [TITLE_COL, DESC_COL, TIMESTAMP_COL]
DEFAULT_USER_NAME = "No user"
OPEN_STATUS = "open"
CLOSED_STATUS = "closed"
SPECIAL_EDIT_COLUMNS = {
    ASSIGNED_TO_COL: [DEFAULT_USER_NAME],
    STATUS_COL: [OPEN_STATUS, CLOSED_STATUS],
}
ISSUE_TRACKER_TABLE_NAME = "issue_tracker"
TITLE_TYPE = str
DESC_TYPE = str
TIMESTAMP_TYPE = TIME_TYPE
ASSIGNED_TO_TYPE = str
STATUS_TYPE = str
#------------------------------------------------------------------------------------------------

class CIssueTracker(DDatabase):
    '''
    Base class for an issue tracking database.
    '''
    def __init__(self, path, overwrite=False):
        super(CIssueTracker, self).__init__(path, overwrite=overwrite)
        #make the issue tracking table if it doesnt already exist.
        self._issueTableName = ISSUE_TRACKER_TABLE_NAME
        if self._hasTable(self._issueTableName)==False:
            super(CIssueTracker, self).createTable(ISSUE_TRACKER_TABLE_NAME, [
                (TITLE_COL, TIME_TYPE),
                (DESC_COL, DESC_TYPE),
                (TIMESTAMP_COL, TIMESTAMP_TYPE),
                (ASSIGNED_TO_COL, ASSIGNED_TO_TYPE),
                (STATUS_COL, STATUS_TYPE),
            ])

    def __iter__(self):
        '''Iterate through the IDs present in the issue table.'''
        command = "SELECT "+ID_COL_NAME+" from "+self._issueTableName
        ret = self._runCommand(command)
        for r in list(ret):
            yield r[0]

    #------------------------------------------------------------------------------------------------
    # Public members
    #------------------------------------------------------------------------------------------------

    def addData(self, table_name, data_tup):
        '''Overwritten function that returns an error.'''
        msg.message.logError("Uncheck data cannot be added to this database. Use 'newIssue'",
                             "CIssueTracker::createTable")
        ut.exit(1)

    def closeIssue(self, id):
        '''Close the issue.'''
        self.setStatus(id, CLOSED_STATUS)

    def createTable(self, name, columns=[]):
        '''
        Overwritten function that returns an error as no more tables should be added to this database.
        '''
        msg.message.logError("No new tables can be added to this dataase.", "CIssueTracker::createTable")
        ut.exit(1)

    def deleteIssue(self, id):
        '''Remove an issue from the database.'''
        self.deleteData(self._issueTableName, id)

    def getIssue(self, id):
        '''Returns a read only issue object that does not update in real time.'''
        cols = [ID_COL_NAME, TITLE_COL, DESC_COL, TIMESTAMP_COL, ASSIGNED_TO_COL, STATUS_COL]
        data = self.getDataList(self._issueTableName, cols, id)
        if len(data) != len(cols): return
        return DIssue(*data)

    def getIssueList(self):
        '''Returns a lsit of strings that show issue info. Used to display on the server.'''
        #iterate over the ids, get and issue and make an str
        rets = []
        for id in self:
            io = self.getIssue(id)
            rets.append(str(io))
        rstr = ""
        for ret in rets:
            rstr += ret+"\n"
        ret =  rstr.replace("\n","<br>")
        print(ret)
        return ret

    def newIssue(self, title, descrip, assigned_to=DEFAULT_USER_NAME, status=OPEN_STATUS):
        '''Add a new issue to the database.'''
        ts = ut.getStringTime()
        self._checkData(title, descrip, ts, assigned_to, status)
        #make tuple of data
        data_tup = [title, descrip, ts, assigned_to, status]
        return super(CIssueTracker, self).addData(self._issueTableName, data_tup)

    def openIssue(self, id):
        '''Open the issue.'''
        self.setStatus(id, OPEN_STATUS)

    def setStatus(self, id, status):
        '''Set the issue status.'''
        if self._checkStatus(status)==False:
            msg.message.logError("Given 'status' value '" + str(status) + "' is not an accepted status.",
                                 "CIssueTracker::setStatus")
            ut.exit(1)
        self.updateCell(self._issueTableName, id, STATUS_COL, status)

    def updateCell(self, tab_name, row_id, col_name, new_val):
        '''
        Makes sure the given column is not read only, checks some special values, then runs the parent
        function.
        '''
        if col_name in READ_ONLY_COLUMNS:
            msg.message.logError(col_name+" column is read only.","CIssueTracker::updateCell")
            ut.exit(1)
        if col_name in SPECIAL_EDIT_COLUMNS.keys():
            if not new_val in SPECIAL_EDIT_COLUMNS[col_name]:
                msg.message.logError(col_name + " column cannot have value '"+str(new_val)+"'.",
                                     "CIssueTracker::updateCell")
                ut.exit(1)
        super().updateCell(tab_name, row_id, col_name, new_val)

    #------------------------------------------------------------------------------------------------
    # Private members
    #------------------------------------------------------------------------------------------------

    def _checkData(self, title, descrip, ts, assigned_to, status):
        if not isinstance(title, TITLE_TYPE) or \
            not isinstance(descrip, DESC_TYPE) or \
            not isinstance(ts, TIMESTAMP_TYPE) or \
            not isinstance(assigned_to, ASSIGNED_TO_TYPE):
            msg.message.logError("One or more of the given data types are not correct.",
                                 "CIssueTracker::_checkData")
            ut.exit(1)
        if assigned_to not in SPECIAL_EDIT_COLUMNS[ASSIGNED_TO_COL]:
            msg.message.logError("Given 'assigned_to' value '"+str(assigned_to)+"' is not an accepted user.",
                                 "CIssueTracker::_checkData")
            ut.exit(1)

        if self._checkStatus(status)==False:
            msg.message.logError("Given 'status' value '" + str(status) + "' is not an accepted status.",
                                 "CIssueTracker::_checkData")
            ut.exit(1)

    def _checkStatus(self, status):
        if not isinstance(status, STATUS_TYPE): return False
        if status not in SPECIAL_EDIT_COLUMNS[STATUS_COL]: return False
        return True

