'''
Classes for message logging.

Kevin Maguire
26/03/19

'''

#------------------------------------------------------------------------------------------------

import sys

#------------------------------------------------------------------------------------------------

DEBUG = 4
INFO = 3
WARN = 2
ERROR = 1
NONE = 0
DEFAULT_MESSAGE_LEVEL = DEBUG

#------------------------------------------------------------------------------------------------

class CMessage(object):
    '''
    'Static' class for message output to terminal.
    '''
    def __init__(self, set_level=DEFAULT_MESSAGE_LEVEL):

        self._checkLevel(set_level)
        self._level = set_level

    #------------------------------------------------------------------------------------------------
    # Public functions
    #------------------------------------------------------------------------------------------------

    def checkLevel(self, lvl):
        '''
        Returns True if the given lvl is less than or equal to the set level and thus the message should be shown.
        '''
        if lvl <= self._level: return True
        return False

    def displayBreak(self, lvl=None):
        if lvl == None: lvl = self._level
        self.log("-"*80, lvl)

    def log(self, message, lvl, fname=None):
        '''Log a message with the given status.'''
        return self._log(message, lvl, fname=fname)

    def logDebug(self, message, fname=None):
        '''Log a message with the DEBUG status.'''
        return self._log(message, DEBUG, fname=fname)

    def logError(self, message, fname=None):
        '''Log a message with the ERROR status.'''
        return self._log(message, ERROR, fname=fname)

    def logInfo(self, message, fname=None):
        '''Log a message with the INFO status.'''
        return self._log(message, INFO, fname=fname)

    def setLevel(self, lvl):
        '''Change the message level.'''
        self._checkLevel(lvl)
        self._level = lvl

    #------------------------------------------------------------------------------------------------
    # Private functions
    #------------------------------------------------------------------------------------------------

    def _checkLevel(self, lvl):
        '''
        Check the given level is valid. Gives a fatal error if there is a problem with the level.

        :param lvl: An integer level between DEBUG and NONE.
        '''
        if not isinstance(lvl, int):
            print("ERRO: CMessage::_checkLevel: Given mesage level must be an integer.")
            sys.exit(1)
        if lvl > DEBUG or lvl < NONE:
            print("ERRO: CMessage::_checkLevel: Given mesage int must be between "+str(DEBUG)+" and "+str(NONE)+".")
            sys.exit(1)

    def _getLevelString(self, lvl):
        '''Returns a string that represents the given lvl.'''
        if lvl == DEBUG: return "DEBG"
        elif lvl == INFO: return "INFO"
        elif lvl == WARN: return "WARN"
        elif lvl == ERROR: return "ERRO"
        else:
            print("ERRO: CMessage::_getLevelString: Given level("+str(lvl)+") not valid.")
            sys.exit(1)

    def _log(self, message, lvl, fname=None):
        '''
        Show the given mesage to the terminal if the level is less than the set level.

        :param message: The string text of the message.
        :param lvl: The integer level of this message.
        :param fname: If a string is given, it is printed before the message. Give the calling function name.
        '''
        if self.checkLevel(lvl)==False: return None
        txt = self._getLevelString(lvl)+": "
        if fname != None and isinstance(fname, str):
            txt += fname+": "
        txt += message
        print(txt)
        return txt

message = CMessage(DEFAULT_MESSAGE_LEVEL)
