'''
Classes for utilities.

Kevin Maguire
26/03/19

'''

#------------------------------------------------------------------------------------------------

import os, sys
from datetime import datetime

from .message import message, INFO, ERROR

#------------------------------------------------------------------------------------------------

#------------------------------------------------------------------------------------------------
# Date and time
#------------------------------------------------------------------------------------------------

DATE_TIME_FORMAT = "%Y-%m-%d %H:%M:%S"

def getTime():
    '''Return the date and time as a datetime object.'''
    return convertStringToTime(getStringTime())

def getStringTime():
    '''Return the date and time in a standard string format.'''
    return datetime.now().strftime(DATE_TIME_FORMAT)

def convertStringToTime(time_string):
    '''Given the standard string format, convert it to a time object.'''
    return datetime.strptime(time_string, DATE_TIME_FORMAT)

#------------------------------------------------------------------------------------------------
# System utilities
#------------------------------------------------------------------------------------------------

def exit(code):
    '''
    Exit the system with given code. 0 is success, 1 is failure.
    '''
    if code == 0:
        message.log("Exiting program with success status.", INFO, "utilities::exit")
        sys.exit(0)
    elif code == 1:
        message.log("Exiting program with failure status.", ERROR, "utilities::exit")
        sys.exit(1)
    else:
        message.log("Unknown exit code.", ERROR, "utilities::exit")
        exit(1)

def isFile(path):
    '''Check if the given path is the path to a file.'''
    return os.path.isfile(path)

def deleteFile(path):
    '''Deletes the file at the given path.'''
    if isFile(path)==False:
        message.logError("Given file does not exist.")
        exit(1)
    os.remove(path)