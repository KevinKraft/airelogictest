'''
Classes for databases.

Kevin Maguire
26/03/19

'''

#------------------------------------------------------------------------------------------------

import sqlite3
import pandas as pd

import util.message as msg
import util.utilities as ut

#------------------------------------------------------------------------------------------------

TIME_TYPE = str
TEXT_TYPE = "TEXT"
INT_TYPE = "INT"
ID_COL_NAME = "ID"

#------------------------------------------------------------------------------------------------

class DDatabase(object):
    '''
    Base class for any type of database.

    .. todo:: Read the initial db to set a dict of column names and types, this is also done when making \
    cols. This is done so I know how to process certain columns. It should be given when making the class.

    '''
    def __init__(self, path, overwrite=False):
        if not isinstance(path, str):
            msg.message.log("Given path must be a string.",msg.ERROR, "DDatabase::__init__")
            ut.exit(1)
        if not isinstance(overwrite, bool):
            msg.message.log("Given 'overwrite' must be a bool.",msg.ERROR, "DDatabase::__init__")
            ut.exit(1)
        self._overwrite = overwrite
        self._path = path
        #connect to db
        self._db, self._tableInfo = self._connectDB()

    def __str__(self):
        '''Print some info.'''
        ret = "Database: \n"
        for key, val in self._tableInfo.items():
            ret += "    Table: "+key+"\n"
            for ival in val:
                ret += "        ("+ival[0]+","+str(ival[1])+")\n"
        return ret

    def __del__(self):
        self._db.close()

    #------------------------------------------------------------------------------------------------
    # Public members
    #------------------------------------------------------------------------------------------------

    def addData(self, table_name, data_tup):
        '''
        Add data to a table by name.

        :param data_tup: A tuple of data to be inserted for each column value, in order.
        :param table_name: The string name of the table.
        :return: Returns the new ID of the row.
        '''
        self._checkTableName(table_name)
        command = "INSERT INTO "+table_name+" ("
        #check size, -1 for ID
        if len(data_tup) != len(self._tableInfo[table_name])-1:
            msg.message.logError("Given tuple of data values is the wrong length.", "DDatabase::addData")
            ut.exit(1)
        #get next ID
        nid = self._getNextID(table_name)
        data_list = list(data_tup)
        data_list.insert(0, nid)
        #add the names of the columns of this table
        val_str = ""
        for cellval, coldata in zip(data_list, self._tableInfo[table_name]):
            command += coldata[0]+","
            val_str += self._getCellDataStr(cellval, coldata[1])
            val_str+=","
        command = command[:-1]
        command += ") VALUES ("+val_str[:-1]+")"
        self._runCommand(command)
        self._update()
        return nid

    def createTable(self, name, columns=[]):
        '''
        Create a table with the given name in the database. Note that an ID entry is created
        automatically.

        :param name: A string name for the table that must be unique in the DB.
        :param columns: A list of tuples of (column name, type)
        '''
        if not isinstance(name, str):
            msg.message.logError("Given name must be a string.","DDatabase::createTable")
            ut.exit(1)
        if not isinstance(columns, list):
            msg.message.logError("Given columns must be a list.", "DDatabase::createTable")
            ut.exit(1)
        for tup in columns:
            if not isinstance(tup, tuple):
                msg.message.logError("Given columns must be a list of tuples.", "DDatabase::createTable")
                ut.exit(1)
            if len(tup) != 2:
                msg.message.logError("Given columns must be a list of tuples of length two.",
                                     "DDatabase::createTable")
                ut.exit(1)
        #start the command string
        command = "CREATE TABLE "+name
        #make ID column
        command += "("+ID_COL_NAME+" INT PRIMARY KEY NOT NULL"
        for col in columns:
            command += ", "+col[0]+" "+self._getSQLTypeString(col[1])+" NOT NULL"
        command += ");"
        self._db.execute(command)
        self._update()
        msg.message.logDebug("Table with name '"+name+"' created.","DDatabase::createTable")

    def dataframe(self, tab_name):
        '''Returns a pandas dataframe of the database from given table name.'''
        self._checkTableName(tab_name)
        return pd.read_sql_query("SELECT * from "+tab_name, self._db)

    def deleteData(self, tab_name, row_id):
        '''Delete the data with ID equal to the given row ID.'''
        if not isinstance(row_id,int):
            msg.message.logError("Given row_id must be an int.","DDatabase::deleteData")
            ut.exit(1)
        if self._checkRowID(tab_name, row_id)==False: return
        self._checkTableName(tab_name)
        #check that this entry exists.
        if self.hasData(tab_name, ID_COL_NAME, row_id)==False:
            msg.message.logError("Given row_id("+str(row_id)+") does not specify a row in the DB.",
                                 "DDatabase::deleteData")
            ut.exit(1)
        #write the command
        coltype = self._getColType(tab_name, ID_COL_NAME)
        val = self._getCellDataStr(row_id, coltype)
        command = "DELETE from "+tab_name+" where ID="+val+";"
        self._runCommand(command)
        msg.message.logDebug("Deleted row "+str(row_id)+" from table "+tab_name+".",
                             "DDatabase::deleteData")
        self._update()

    def getData(self, tab_name, col_name, row_id):
        '''Returns the value in the column of the given table in in the given row'''
        self._checkTableName(tab_name)
        self._checkColName(tab_name, col_name)
        self._checkRowID(tab_name, row_id)
        if self.hasData(tab_name, ID_COL_NAME, row_id)==False:
            msg.message.logError("Given row_id("+str(row_id)+") does not specify a row in the DB.",
                                 "DDatabase::getData")
            ut.exit(1)
        id_str = self._getStrValForCol(tab_name, ID_COL_NAME, row_id)
        command = "SELECT "+col_name+" from "+tab_name+" where "+ID_COL_NAME+"="+id_str
        return list(self._runCommand(command))[0][0]

    def getDataList(self, tab_name, col_list, row_id):
        '''Returns a list of the data in the cells of row_id for the given column headers, in oder.'''
        res = []
        if self._checkRowID(tab_name, row_id)==False: return []
        for col in col_list:
            res.append( self.getData(tab_name,col,row_id) )
        return res

    def hasData(self, table_name, col_name, val):
        '''Check if the table of the given name has a row with the given column name and value.'''
        self._checkTableName(table_name)
        self._checkColName(table_name, col_name)
        cursor = self._db.cursor()
        coltype = self._getColType(table_name, col_name)
        val_str = self._getCellDataStr(val, coltype)
        ret = list(cursor.execute("SELECT EXISTS(SELECT 1 FROM "+
                                  table_name+" WHERE "+col_name+"="+val_str+");"))[0][0]
        return 1 == ret

    def updateCell(self, tab_name, row_id, col_name, new_val):
        '''Update the cell data in the given table at the given row ID.'''
        if col_name == ID_COL_NAME:
            msg.message.logError("ID column is read only.",
                                 "DDatabase::updateCell")
            ut.exit(1)
        self._checkTableName(tab_name)
        if self._checkRowID(tab_name, row_id)==False: return
        self._checkColName(tab_name, col_name)
        if self.hasData(tab_name, ID_COL_NAME, row_id)==False:
            msg.message.logError("Given row_id("+str(row_id)+") does not specify a row in the DB.",
                                 "DDatabase::updateCell")
            ut.exit(1)
        val_str = self._getStrValForCol(tab_name, col_name, new_val)
        id_str = self._getStrValForCol(tab_name, ID_COL_NAME, row_id)
        command = "UPDATE "+tab_name+" set "+col_name+"="+val_str+" where "+ID_COL_NAME+"="+id_str+";"
        ret = self._runCommand(command)
        self._update()

    #------------------------------------------------------------------------------------------------
    # Protected members
    #------------------------------------------------------------------------------------------------

    def _hasTable(self, name):
        return name in self._tableInfo.keys()

    def _runCommand(self, command):
        msg.message.logDebug("Running command: "+command, "DDatabase::_runCommand")
        return self._db.cursor().execute(command)

    #------------------------------------------------------------------------------------------------
    # Private members
    #------------------------------------------------------------------------------------------------

    def _checkColName(self, tab_name, col_name):
        '''
        Check if the given col name for this table is valid.
        '''
        cols = [ d[0] for d in self._tableInfo[tab_name] ]
        if col_name in cols: return True
        return False

    def _checkRowID(self, tab_name, row_id):
        '''Make sure the given ID is a valid ID.'''
        if self.hasData(tab_name, ID_COL_NAME, row_id)==False:
            msg.message.logDebug("Invalid row ID.","DDatabase::_checkRowID")
            return False
        return True

    def _checkTableName(self, table_name):
        '''Checks the table name and gives errors if there is a problem.'''
        if not isinstance(table_name, str):
            msg.message.logError("Given table name must be a string.","DDatabase::_checkTableName")
            ut.exit(1)
        if self._hasTable(table_name)==False:
            msg.message.logError("Given table name does not exist in the database.",
                                 "DDatabase::_checkTableName")
            ut.exit(1)

    def _connectDB(self):
        '''Connect to the sqlite3 database file.'''
        msg.message.logDebug("Connecting to database '"+self._path+"'.","DDatabase::_connectDB")
        if self._overwrite == True:
            ut.deleteFile(self._path)
        db = sqlite3.connect(self._path)
        db.commit()
        msg.message.logDebug("Connected to database '"+self._path+"'.","DDatabase::_connectDB")
        #also return the table info dict.
        table_info = self._getTableInfoDict(db)
        return db, table_info

    def _getCellDataStr(self, cellval, coldata_type):
        '''Puts the str value in the correct format.'''
        val_str = ""
        if coldata_type == INT_TYPE:
            val_str = str(cellval)
        elif coldata_type == TEXT_TYPE:
            val_str = "'" + str(cellval) + "'"
        else:
            msg.message.logError("Unknown column data type('" + str(coldata_type) + "').",
                                 "DDatabase::_getCellDataStr")
            ut.exit(1)
        return val_str

    def _getColType(self, tab_name, col_name):
        for tup in self._tableInfo[tab_name]:
            if tup[0]==col_name: return tup[1]
        msg.message.logError("Given column name not valid.","DDatabase::_getColType")
        ut.exit(1)

    def _getNextID(self, tab_name):
        '''Return the next row index (accounts for indexing from zero).'''
        return self._getNRows(tab_name)

    def _getNRows(self, tab_name):
        '''Get the number of rows in the table of the given name,'''
        cursor = self._db.cursor()
        return list(cursor.execute("SELECT COALESCE(MAX(id)+1, 0) FROM " + tab_name))[0][0]

    def _getTableInfoDict(self, db):
        '''Returns a dict of table name keys and a list of tuples of column names and types as the value.'''
        cursor = db.cursor()
        tdict = {}
        for tab in cursor.execute("SELECT * FROM sqlite_master WHERE type='table';"):
            #key name, value list of types
            tdict[tab[1]] = []
            for tup in list(cursor.execute("PRAGMA table_info("+tab[1]+")")):
                tdict[tab[1]].append( (tup[1], tup[2]) )
        return tdict

    def _getSQLTypeString(self, type_str):
        '''Return the string for the given type that goes into the SQL command.'''
        if type_str == int:
            return INT_TYPE
        elif type_str == str:
            return TEXT_TYPE
        elif type_str == TIME_TYPE:
            return TEXT_TYPE
        else:
            msg.message.logError("Given type name '"+str(type_str)+"' not recognised.")
            ut.exit(1)

    def _getStrValForCol(self, tab_name, col_name, val):
        coltype = self._getColType(tab_name, col_name)
        return self._getCellDataStr(val, coltype)

    def _update(self):
        self._db.commit()
        #remake the info dict
        self._tableInfo = self._getTableInfoDict(self._db)